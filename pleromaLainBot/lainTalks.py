from mastodon import Mastodon
import json
import random
import os

mastodon = Mastodon(
        access_token = 'lain_usercred.secret',
        api_base_url = 'https://kiwifarms.cc'
)
def sendQuote():
    
    with open('lainQuotes.json') as json_file:
        quotes = json.load(json_file)
    randomNumber = random.randint(0, len(quotes['quotes']))
    created = mastodon.toot(quotes['quotes'][randomNumber])
    print(created["id"])

def sendImage():
    images = os.listdir('images')
    randomNumber = random.randint(0, len(images))
    imagePath = os.getcwd() + '/images/' + images[randomNumber]
    media = mastodon.media_post(imagePath)
    mastodon.status_post('', None,media)


if random.randint(0,2) == 1:
    sendQuote()
else:
    sendImage()
